<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Workflow
 * @package App
 * @author pavlos
 */
class Workflow extends Model
{
    /**
     * The table name
     *
     * @var string
     */
    protected $table = 'workflow';

    /**
     * @var array
     */
    protected $fillable = ['start_at', 'end_at', 'type', 'comment', 'duration', 'confirmed'];

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id')->withoutGlobalScope('active');
    }

}
