<?php

namespace App\Http\Controllers\Api;

use App\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Workflow;

/**
 * Class WorkflowController
 * @package App\Http\Controllers\Api
 * @author pavlos
 */
class WorkflowController extends Controller
{

    /** @SWG\Get(
     *     path="/api/v1/user/workflow",
     *     description="Get Workflows List",
     *     summary="Workflow List",
     *     operationId="workflow.list",
     *     tags={"workflow"},
     *     @SWG\Response(
     *         response=422,
     *         description="Validation error"
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Token is incorrect"
     *     ),
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Manager token",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="all",
     *         in="path",
     *         description="true for ADMIN",
     *         type="boolean",
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="path",
     *         description="start date",
     *         type="string",
     *     )
     * )
     **/
    public function getWorkflow(Request $request)
    {
        $start = new \DateTime($request->date);
        $end = clone($start);
        $interval = new \DateInterval('P1M');
        $end->add($interval);
        $all = filter_var($request->all, FILTER_VALIDATE_BOOLEAN);

        $data = Workflow::where('start_at', '>=', $start->format('Y-m-01'));

        if (!($all && $request->user()->hasRole(UserRole::ROLE_ADMIN))) {
            $data = $data
                ->where('start_at', '<', $end->format('Y-m-01'))->where('author_id', $request->user()->id);
        }

        /**
         * @todo Check access
         */

        return response()->json($data->get());
    }
}
