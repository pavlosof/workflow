## Workflow module for the Reporter

> **Note:** This repository contains the core code, routes and migrations of Workflow API-s 

### Setup
Run the following command in your terminal
```
composer require pavlosof/workflow
```
Add to config\app.php, array «providers» following string:
```
Pvl\Workflow\WorkflowServiceProvider::class,
```
Run the following command in your terminal
```
php artisan vendor:publish  --provider="Pvl\Workflow\WorkflowServiceProvider"
```
Make
```
php artisan migrate
```

### License

It's open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
