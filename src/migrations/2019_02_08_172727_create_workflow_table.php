<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkflowTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'workflow';

    /**
     * Run the migrations.
     * @table workflow
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('type', ['working_off', 'time_off', 'vacation', 'sick_leave'])->nullable()->default(null);
            $table->unsignedInteger('author_id');
            $table->date('start_at');
            $table->date('end_at')->nullable()->default(null);
            $table->mediumText('comment')->nullable()->default(null);
            $table->tinyInteger('confirmed')->default('1');
            $table->integer('duration')->default('480');

            $table->index(['author_id'], 'author_id_workflow');

            $table->unique(['start_at', 'author_id', 'deleted_at'], 'workflow_start_at_author_id_deleted_at_unique');
            $table->softDeletes();
            $table->nullableTimestamps();

            $table->foreign('author_id', 'author_id_workflow')->references('id')->on('users')->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}