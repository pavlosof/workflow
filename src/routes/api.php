<?php

Route::group([
    'namespace' => 'App\Http\Controllers\Api',
    'prefix' => 'api'
], function () {
    Route::group(['prefix' => 'v1'], function () {

        Route::group(['middleware' => ['check_user_role:' . \App\UserRole::ROLE_USER]], function () {

            Route::group(['prefix' => 'user'], function () {
                Route::get('workflow', 'WorkflowController@getWorkflow');
            });

        });
    });
});